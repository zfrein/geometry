﻿using System;
using System.Linq;

namespace Geometry
{
    /// <summary>
    /// Сервис для работы с треугольками
    /// </summary>
    public class TriangleService
    {
        //по заданию метод должен примать все 3 стороны
        //в задаче не определены другие условия использования этой библиотеки, 
        //поэтому все вспомогательные методы будут приватными, хотя некоторые могли бы использоваться из вне


        private readonly double tolerance;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public TriangleService()
        {
            tolerance = 0.00001d;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="tolerance">Допустимая погрешность</param>
        public TriangleService(double tolerance)
        {
            this.tolerance = tolerance;
        }

        /// <summary>
        /// Рассчитываем площадь прямугоьлнико треугольника по 3-м сторонам
        /// </summary>
        /// <param name="a">1-я сторона</param>
        /// <param name="b">2-я сторона</param>
        /// <param name="c">3-я сторона </param>
        /// <exception cref="ArgumentException">Возникает, когда введены не корректные значения длинн сторон, или это не 
        /// треугольник, или это не прямогульный треугольник</exception>
        /// <returns>Возвращает площадь треугольника</returns>
        public double GetRightTriangleArea(double a, double b, double c)
        {

            #region [ Precondition ]
            var sides = (new[] { a, b, c }).OrderByDescending(x => x).ToArray();

            if (sides[2] <= 0)
                throw new ArgumentException("Сторона треугольника  может иметь только положительную длинну");

            if (!this.CheckTriangleRule(a, b, c))
                throw new ArgumentException("Переданные значения не могут быть сторонами треугольника");

            //определяем потенциальные катеты(если треугольник не прямоугольны то и не катеты они вовсе)
            var cathetus1 = sides[1];
            var cathetus2 = sides[2];
            var hypotenuse = sides[0];

            if (!this.IsRightTriangle(cathetus1, cathetus2, hypotenuse))
                throw new ArgumentException("Переданные значения не могут быть сторонами прямоугольного треугольника");

            #endregion 

            return GetRightTriangleArea(cathetus1, cathetus2);
         }

        /// <summary>
        /// Проверяет, могут ли переданные значения являться длиннами сторон треугольника по Правилу Треугольника
        /// </summary>
        /// <param name="a">1-я сторона</param>
        /// <param name="b">2-я сторона</param>
        /// <param name="c">3-я сторона </param>
        /// <returns></returns>
        private bool CheckTriangleRule(double a, double b, double c)
        {
            return a < (b + c) && b < (a + c) && c < (a + b);
        }

        /// <summary>
        /// Считаем площадь треугольника по 2-м катетам
        /// </summary>
        /// <param name="a">1-й катет</param>
        /// <param name="b">2-й катет</param>
        /// <remarks>Самый оптимальный способ вычисесления площади прямоугольного треугольника</remarks>
        /// <returns>Площадь треугольника</returns>
        private double GetRightTriangleArea(double a, double b)
        {
            return a * b / 2D;
        }

        /// <summary>
        /// Проверяем, является ли треугольник прямоугольным
        /// </summary>
        /// <param name="cathetus1">Катет</param>
        /// <param name="cathetus2">Катет</param>
        /// <param name="hypotenuse">Гипотенуза</param>
        /// <returns></returns>
        private bool IsRightTriangle(double cathetus1, double cathetus2, double hypotenuse)
        {
            return Math.Abs(Math.Pow(cathetus1, 2) + Math.Pow(cathetus2, 2) - Math.Pow(hypotenuse, 2)) < tolerance;
        }
    }
}
