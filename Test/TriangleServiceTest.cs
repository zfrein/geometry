﻿using System;
using Geometry;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class TriangleServiceTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Сторона треугольника  может иметь только положительную длинну")]
        public void TestNotPositiveSide()
        {
            var service = new TriangleService(0.00001d);
            service.GetRightTriangleArea(2, -3, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Переданные значения не могут быть сторонами треугольника")]
        public void TestNotTriangle()
        {
            var service = new TriangleService(0.00001d);
            service.GetRightTriangleArea(1, 1, 3);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Переданные значения не могут быть сторонами прямоугольного треугольника")]
        public void TestNotRightTriangle()
        {
            var service = new TriangleService(0.00001d);
            service.GetRightTriangleArea(1, 2.5, 3);
        }

        [TestMethod]
        public void TestTriangleArea()
        {
            var service = new TriangleService(0.00001d);
            var rightTriangleArea = service.GetRightTriangleArea(3, 4, 5);
            Assert.AreEqual(rightTriangleArea, 6D);
        }

    }
}
